import datetime

import pandas as pd
import pytz
from ics import Calendar, Event


# Divise un dataframe en un tableau de dataframes de columns_per_group colonnes chacun
def split_dataframe(original_dataframe, columns_per_group):
    grouped_dataframes = []

    for i in range(len(original_dataframe.columns) // columns_per_group):
        grouped_dataframes.append(original_dataframe[original_dataframe.columns[
                                                     columns_per_group * i:columns_per_group * i + columns_per_group]])

    return grouped_dataframes


def remove_unnecessary_values(d):
    d = d.dropna()
    col_name = d.columns[1]
    d = d[d[col_name] != 'Férié']

    # Supprime les entrées dont la première colonne n'est pas une date
    s = pd.to_datetime(d[d.columns[0]], format='%d-%m-%Y', errors='coerce')
    d = d[s.notna()]

    return d


def to_timestamp(dt):
    return int(datetime.datetime.timestamp(dt))


def make_uid(start_datetime, end_datetime):
    return f'CESI.MSI.{to_timestamp(start_datetime)}-{to_timestamp(end_datetime)}'


def make_event(dataframe_row, timezone):
    date = timezone.localize(dataframe_row[0])
    start = date.replace(hour=8, minute=45, second=0)
    end = date.replace(hour=16, minute=45, second=0)

    event = Event()
    event.uid = make_uid(start, end)
    event.name = dataframe_row[1]
    event.begin = start
    event.end = end

    return event


def make_ics_file(dataframes_list, target_file):
    calendar = Calendar()
    timezone = pytz.timezone("Europe/Paris")

    for dataframe in dataframes_list:
        for row_index in range(len(dataframe)):
            row = dataframe.iloc[row_index]
            event = make_event(row, timezone)
            calendar.events.add(event)

    with open(target_file, 'w') as ics_file:
        ics_file.writelines(calendar)


def generate_ics(source_file, target_file):
    xlsx_calendar = pd.read_excel(source_file)

    grouped_dataframes = split_dataframe(xlsx_calendar, 2)

    grouped_dataframes = [remove_unnecessary_values(d) for d in grouped_dataframes]
    grouped_dataframes = [df for df in grouped_dataframes if not df.empty]

    make_ics_file(grouped_dataframes, target_file)


if __name__ == '__main__':
    generate_ics('/home/olivier/work/school/cesi/msi/planningMSI21-22_12M_apprenants_v4.xlsx', 'my.ics')
